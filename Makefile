#Makefile pour le TP Morpho répartie : pour l'utiliser, nommer vos programmes
#principaux de chaque question comme suit : q1.c, q2.c, ...

CC = gcc -c -g --std=c99 -Wall

tout: q1 client client.o q1.o Reseau.o Serveur Serveur.o q3.o q3

#Programmes
Reseau.o: Reseau.c Reseau.h 
	$(CC) Reseau.c


q1: q1.o
	gcc -o q1 q1.o

q1.o: q1.c
	$(CC) q1.c

q3: q3.o
	gcc -o q3 q3.o

q3.o: q3.c
	$(CC) q3.c

client: client.o Reseau.o
	gcc -o client client.o Reseau.o -lpthread

client.o: client.c Reseau.h
	$(CC) client.c

Serveur: Serveur.o Reseau.o
	gcc -o Serveur Serveur.o Reseau.o -lpthread

Serveur.o:Serveur.c Reseau.h
	$(CC) Serveur.c

#Ménage
clean: 
	rm -f *.o q1 client *~

