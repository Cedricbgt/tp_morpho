/* ---------------------------------------------------------------------------
 * Programme client utilisant des sockets en mode STREAM (TCP).
 * Côté client : lecture de lignes au clavier et envoi au serveur jusqu'à lire
 *               une ligne vide, lecture et affichage des réponses du serveur.
 * Auteur     : Damien Genthial
 * Création   : Décembre 2002, adapté en 2005 pour affichage dans une fenêtre
 * 	       séparée. Méthode : lancer un xterm, y taper tty et relever le
 * 	       nom du pseudo-terminal, qu'on peut alors passer en paramètre 
 * 	       4 ici.
 * Entrées    : Lit des lignes au clavier
 * Sorties    : Affiche la réponse du serveur sur l'écran
 * Paramètres : voir usage.
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <pthread.h>
#include "Reseau.h"

void usage(void);
void sortie(const char message[]);
void* recopie(void*);

/* Taille des tampons de lecture et écriture */
#define MAX 128

/* Structure à passer en paramètre des threads de recopie */
typedef struct {
    int in;
    int out;
} Parametre;

int main(int nbArg, char *arg[])
{

    int sock = socketServer(6127, TCP);
    if(sock==-1){
        perror("error");
        return EXIT_FAILURE;
    }
    int socketFD = accept(sock,NULL,NULL);

     pid_t ident;
    int tube_pere_fils[2];      /* les deux descripteurs de fichier associés au tube */
    int tube_fils_pere[2];
    char msg[MAX];
    int k = 0;

    /* Création du tube */
    if (pipe(tube_pere_fils) != 0) {
        perror("pipe creation failed");
        return EXIT_FAILURE;
    }

    if (pipe(tube_fils_pere) != 0) {
        perror("pipe creation failed");
        return EXIT_FAILURE;
    }

    /* Création du fils */
    ident = fork();
    switch (ident) {
    case -1:
        perror("fork");
        return EXIT_FAILURE;

    case 0:                    /* Fils */

        close(tube_pere_fils[1]);
        close(tube_fils_pere[0]);
        dup2(tube_pere_fils[0], 0);
        dup2(tube_fils_pere[1], 1);
        execlp("morpho", "morpho", NULL);
        break;

    default:                   /* Père */

        close(tube_pere_fils[0]);
        close(tube_fils_pere[1]);
        while (1) {
            read(socketFD, msg, MAX);
            if (strlen(msg) == 1) {
                printf("vide\n");
                break;
            }
            write(tube_pere_fils[1], msg, strlen(msg));
            usleep(1000);
            k = read(tube_fils_pere[0], msg, MAX);
            msg[k] = '\0';
            write(socketFD, msg, strlen(msg));
        }
    }

    return EXIT_SUCCESS;
    close(socketFD);
}



